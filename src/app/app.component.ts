import {Component} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, debounceTime, distinctUntilChanged, Observable, pluck, share, switchMap} from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  gameDisplayed$: BehaviorSubject<number> = new BehaviorSubject<number>(1);

  filteredInput$: Observable<number> = this.gameDisplayed$.pipe(
    distinctUntilChanged(),
    debounceTime(100)
  );

  gameData$: Observable<any> = this.filteredInput$.pipe(
    debounceTime(100),
    switchMap(gameNum => this.client.get(`assets/rawdata/${gameNum}.json`, {responseType: "json"})),
    share()
  );

  location$: Observable<string> = this.pluckGameData("location");
  blueData$: Observable<any> = this.pluckGameData("private", 0, "field");
  redData$: Observable<any> = this.pluckGameData("private", 1, "field");
  wins$: Observable<number> = this.pluckGameData("wins", 0)
  loses$: Observable<number> = this.pluckGameData("wins", 1)

  constructor(private client: HttpClient) {
  }

  pluckGameData(...args: (string | number)[]): Observable<any> {
    return this.gameData$.pipe(
      pluck(...args as any)
    );
  }

  handleInput(event: any) {
    this.gameDisplayed$.next(parseInt(event.target.value, 10));
  }
}
