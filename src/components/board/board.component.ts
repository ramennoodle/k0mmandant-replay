import {Component, ElementRef, Input, OnChanges, SimpleChanges} from "@angular/core";

@Component({
    selector: "app-board",
    template: "",
    styleUrls: ["./board.component.scss"]
})
export class BoardComponent implements OnChanges {
  @Input()
  blueField: number[] = Array(100).fill(0);
  @Input()
  redField: number[] = Array(100).fill(0);
  @Input()
  location: string = "_____";
  @Input()
  wins: string = "___";
  @Input()
  loses: string = "___";

  constructor(private elementRef: ElementRef) {
    this.redraw();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.redraw();
  }

  redraw() {
    if (!this.location || !this.wins || !this.loses || !this.blueField || !this.redField) {
      return;
    }
    this.clear();
    this.draw();
  }

  draw(): void {
    this.blitLines(this.generateHeader([this.location, this.wins, this.loses]));
    this.blitLines(this.generateLinesFromData());
    this.blitLines(this.generateFooter());
  }

  blitLines(lines: string[]) {
    lines.forEach(l => {
      const div = document.createElement("div");
      div.textContent = l;
      this.elementRef.nativeElement.appendChild(div);
    });
  }

  mapFieldValueToMarker(val: number): string {
    switch (val) {
      case 3: return "><";
      case 2: return "::";
      case 1: return "◄►";
      default: return "  ";
    }
  }

  generateLinesFromData(): string[] {
    const lines: string[] = [];
    if (!this.blueField || !this.redField) {
      return lines;
    }

    for (let i = 0; i < 10; i++) {
      let blueLine = ""
      let redLine = "";

      for (let j = 0; j < 10; j++) {
        const index = j + i * 10;
        const blueVal = this.blueField[index];
        const redVal = this.redField[index];

        blueLine += this.mapFieldValueToMarker(blueVal);
        redLine += this.mapFieldValueToMarker(redVal);
      }

      lines.push(this.generateLine(blueLine, redLine, i));
    }

    return lines;
  }

  clear() {
    let child;
    while (child = this.elementRef.nativeElement.firstChild) {
      this.elementRef.nativeElement.removeChild(child);
    }
  }

  generateHeader(params: string[]) {
    const lines = [
      "╔══════════════════════╦════════════╦═══════════════╦═══════╗",
      "║ EINSATZGEBIET: EEEEE ║ SIEGE: SSS ║ NIEDERL.: NNN ║       ║",
      "╠══════════════════════╩══════╦═════╩═══════════════╩═══════╣",
      "║  ┌────────────────────┐     ║   ┌────────────────────┐    ║",
      "║  │ → BLAU (SCHLAU)    │     ║   │ → ROT (TOT)        │    ║",
      "║  ├────────────────────┤     ║   ├────────────────────┤    ║"
    ];

    lines[1] = lines[1].replace("EEEEE", params[0])
      .replace("SSS", params[1])
      .replace("NNN", params[2]);

    return lines;
  }

  generateLine(fieldsOwn: string, fieldsTarget: string, index: number) {
    const start = "║  │";
    const mid = `├ ${index + 1}${index < 9 ? " " : ""}  ║   │`;
    const end = `├ ${index + 1}${index < 9 ? " " : ""} ║`;
    return `${start}${fieldsOwn}${mid}${fieldsTarget}${end}`;
  }

  generateFooter() {
    const lines = [
      "║  └─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬┘     ║   └─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬┘    ║",
      "║    A B C D E F G H I J      ║     A B C D E F G H I J     ║",
      "╚═══════════════════════════════════════════════════════════╝"
    ];

    return lines;
  }
}
